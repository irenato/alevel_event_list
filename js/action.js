$('body').on('click', '#main-form', function (e) {
    $('#errors-on-main-form').text('');
    e.preventDefault();
    var user_name = $(this).parent().find("input[name='name']"),
        user_second_name = $(this).parent().find("input[name='second-name']"),
        user_email = $(this).parent().find("input[name='email']"),
        user_phone = $(this).parent().find("input[name='phone']"),
        action_name = $(this).attr('data-send'),
        experience = $(this).parent().find("input.experience").prop('checked') ? '1' : '0';
        rex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        rex_phone = /((0|\+3)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
    if (user_name.val() == '' || user_second_name.val() == '' || user_email.val() == '' || user_phone.val() == '') {
        $('#errors-on-main-form').text('Не все поля заполнены');
        $('#errors-on-main-form').show();
    } else if (rex.test(user_email.val()) && rex_phone.test(user_phone.val())) {
        var send_data = $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'registration_to_event',
                'user_name': user_name.val(),
                'user_second_name': user_second_name.val(),
                'user_email': user_email.val(),
                'user_phone': user_phone.val(),
                'experience' : experience,
                'action_name': action_name,
            },
        });
        send_data.done(function () {
            $('.modal.fade.bs-example-modal-lg2').modal('show');
            user_name.val('');
            user_second_name.val('');
            user_email.val('');
            user_phone.val('');
            $(this).attr('data-send', '');
        })
    } else {
        $('#errors-on-main-form').text('Email или номер телефона имеют неверный формат');
        $('#errors-on-main-form').show();
    }
})

$('body').on('click', '#next-form', function (e) {
    $('#errors-on-main-form').hide();
    $('#errors-on-main-form').text('');
    e.preventDefault();
    var user_name = $(this).parent().find("input[name='name']"),
        user_email = $(this).parent().find("input[name='email']"),
        user_second_name = $(this).parent().find("input[name='second-name']"),
        user_phone = $(this).parent().find("input[name='phone']"),
        action_name = $(this).attr('data-send'),
        experience = $(this).parent().find("input.experience").prop('checked') ? '1' : '0';
        rex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        rex_phone = /((0|\+3)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
    if (user_name.val() == '' || user_email.val() == '' || user_phone.val() == '') {
        $('#errors-on-next-form').text('Не все поля заполнены');
        $('#errors-on-next-form').show();
    } else if (rex.test(user_email.val()) && rex_phone.test(user_phone.val())) {
        var send_data = $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'registration_to_event',
                'user_name': user_name.val(),
                'user_second_name': user_second_name.val(),
                'user_email': user_email.val(),
                'user_phone': user_phone.val(),
                'action_name': action_name,
                'experience' : experience,
            },
        });
        send_data.done(function (result) {
            if (result == 'done!') {
                $('.modal.fade.bs-example-modal-lg').modal('hide');
                if (action_name == 'be-a-partner') {
                    $('.modal.fade.bs-example-modal-lg4').modal('show');
                }
                else {
                    $('.modal.fade.bs-example-modal-lg2').modal('show');
                }
                user_name.val('');
                user_second_name.val('');
                user_email.val('');
                user_phone.val('');
                $(this).attr('data-send', '');
                $('div.registration').find('button.btn').attr('data-send', '');
            }
        })
    } else {
        $('#errors-on-next-form').text('Email или номер телефона имеют неверный формат');
        $('#errors-on-next-form').show();
    }
})

$('body').on('click', '#subscribe-form', function (e) {
    $('#errors-on-main-form').hide();
    $('#errors-on-main-form').text('');
    e.preventDefault();
    var user_name = $(this).parent().find("input[name='name']"),
        user_email = $(this).parent().find("input[name='email']"),
        rex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (user_name.val() == '' || user_email.val() == '') {
        $('#errors-on-next-form').text('Не все поля заполнены');
        $('#errors-on-next-form').show();
    } else if (rex.test(user_email.val())) {
        var send_data = $.ajax({
            url: '/wp-admin/admin-ajax.php',
            type: 'POST',
            data: {
                'action': 'new_subscriber',
                'user_name': user_name.val(),
                'user_email': user_email.val(),
            },
        });
        send_data.done(function (result) {
            if (result == 'done!') {
                $('.modal.fade.bs-example-modal-lg1').modal('hide');
                $('.modal.fade.bs-example-modal-lg6').modal('show');
                user_name.val('');
                user_email.val('');
                $(this).attr('data-send', '');
                $('div.registration').find('button.btn').attr('data-send', '');
            }
        })
    } else {
        $('#errors-on-next-form').text('Email неверного формата');
        $('#errors-on-next-form').show();
    }
})

$('body').on('click', '.register-modal-call', function () {
    if ($(this).attr('data-action-name') == 'register-me')
        $('div.registration div.div-experience').show();
    else
        $('div.registration div.div-experience').hide();
    $('div.registration').find('button.btn').attr('data-send', $(this).attr('data-action-name'));
})

$('body').on('show.bs.modal', '.modal.fade.bs-example-modal-lg2', function () {
    setTimeout(function () {
        $('body').addClass('modal-open');
    }, 500);
});

$('body').on('show.bs.modal', '.modal.fade.bs-example-modal-lg4', function () {
    setTimeout(function () {
        $('body').addClass('modal-open');
    }, 500);
});

$('body').on('show.bs.modal', '.modal.fade.bs-example-modal-lg1', function () {
    setTimeout(function () {
        $('body').addClass('modal-open');
    }, 500);
});

$('body').on('hide.bs.modal', '.modal.fade.bs-example-modal-lg2', function () {
    $('body').removeClass('modal-open');

});

$('body').on('hide.bs.modal', '.modal.fade.bs-example-modal-lg4', function () {
    $('body').removeClass('modal-open');
});

$('body').on('hide.bs.modal', '.modal.fade.bs-example-modal-lg1', function () {
    $('body').removeClass('modal-open');
});

$(document).ready(function () {
    jQuery("#mask").mask("+38?(999) 999-99-99");
    jQuery("#mask1").mask("+38?(999) 999-99-99");
    $('body').on('keypress', '#mask', function () {
        var user_phone_number = $(this).val();
        if (user_phone_number.length > 11) {
            $(this).addClass('perfect-solution');
        }
    });

    $('body').on('keypress', '#mask1', function () {
        var user_phone_number = $(this).val();
        if (user_phone_number.length > 11) {
            $(this).addClass('perfect-solution');
        }
    });
});


