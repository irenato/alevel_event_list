<?php
function froster_scripts()
{

    wp_enqueue_style( 'a_style', get_template_directory_uri() . '/css/style.min.css' );
//    wp_enqueue_style( 'a_fonts', get_template_directory_uri() . '/css/all.min.css' );
//    wp_enqueue_style( 'a_sweet', get_template_directory_uri() . '/css/sweetalert.css' );




    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery-2.2.3.min.js', array(), '', true);//was in header
    if(!is_admin()){wp_enqueue_script( 'jquery' );}


    wp_enqueue_script( 'all_min',  get_template_directory_uri() . '/js/all.min.js', array(), '', true);
    wp_enqueue_script( 'a_google_map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDoFR4O2S0D5Uebjtxa9hCQ3bx2oiIhZz4&amp;libraries=places', array(), '', true);
    wp_enqueue_script( 'all_common',  get_template_directory_uri() . '/js/common.js', array(), '', true);
    wp_enqueue_script( 'a_valid',  get_template_directory_uri() . '/js/jquery.validate.min.js', array(), '', true);
    wp_enqueue_script( 'a_valid_form',  get_template_directory_uri() . '/js/form_validator.js', array(), '', true);
    wp_localize_script( 'a_valid_form', 'alevel_ajax', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
        ));
     wp_enqueue_script( 'a_sweet_js',  get_template_directory_uri() . '/js/sweetalert.min.js', array(), '', true);
}

add_action( 'wp_enqueue_scripts', 'froster_scripts');

function scriptForEvents(){
    wp_enqueue_script( 'all_min',  get_template_directory_uri() . '/js/admin_action.js', array(), '', true);
}

add_action( 'admin_enqueue_scripts', 'scriptForEvents');

function jqForEvents(){
    wp_register_script( 'jquery3', get_template_directory_uri() . '/js/jquery-2.2.3.min.js', false, '', false);
    wp_enqueue_script( 'jquery3' );
}
add_action( 'admin_enqueue_scripts', 'jqForEvents');