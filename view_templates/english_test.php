<?php
/**
 * Template name: englich-test page
 */
?>
<?php get_header();?>

<div class="courses" id='courses'>
	<div class="container">
		<div class="row">
			<script type="text/javascript">

	// Заголовок страницы (h1)
	var title = 'Тест по английскому';
	// Подзаголовок (h2)
	var subtitle = "Этот тест позволяет оценить Ваши знания";
	// Это ваши вопросы
	var questions=[
	{
		text: "Выберите наиболее подходящий ответ! “What does your husband do?”",
		answers: [
		"He is feeding the dog.",
		"He is a doctor",
		"Yes, he does",
		"Yes, he is."
		],
    	correctAnswer: 1 // нумерация ответов с нуля!
    },
    {
    	text: "Что такое альтернативный вопрос в английском языке?",
    	answers: [
    	"Вопрос, требующий ответа «Да» или «Нет».",
    	"Специальный вопрос к любому члену предложения.",
    	"Вопрос, предполагающий выбор между двумя качествами, предметами или действиями.",
    	"Вопрос, являющийся уточнением какого-либо утверждения."
    	],
    	correctAnswer: 2
    },
    {
    	text: "Yesterday I ................. a bird.",
    	answers: [
    	"saw",
    	"sawed",
    	"see",
    	"seed"
    	],
    	correctAnswer: 0
    },
    {
    	text: "Найдите неправильный глагол: to play, to smile, to laugh, to see.",
    	answers: [
    	"to play",
    	"to smile",
    	"to laugh",
    	"to see"
    	],
    	correctAnswer: 3
    },
    {
    	text: "При помощи какого суффикса может образовываться наречие в английском языке? От какой части речи?",
    	answers: [
    	"При помощи суффикса «-ly» от глаголов.",
    	"При помощи суффикса «-ly» от прилагательных.",
    	"При помощи суффикса «-ed» от существительных.",
    	"При помощи суффикса «-ing» от прилагательных."
    	],
    	correctAnswer: 1
    },
    {
    	text: "Укажите существительное, имеющее неправильную форму множественного числа.",
    	answers: [
    	"lady",
    	"gentleman",
    	"son",
    	"daughter"
    	],
    	correctAnswer: 1
    },
    {
    	text: "Найдите ошибку в трёх формах глагола:",
    	answers: [
    	"teach – taught – taught",
    	"catch – caught – caught",
    	"bring – braught – braught",
    	"seek – sought – sought"
    	],
    	correctAnswer: 2
    },
    {
    	text: "Выберите наиболее подходящий ответ! “What is she doing?”",
    	answers: [
    	"She is playing with the bunny.",
    	"She is a manager.",
    	"She cleans the house every day.",
    	"She is clean the carpet."
    	],
    	correctAnswer: 0
    },
    {
    	text: "Как совершается действие, выраженное глаголом в Present Continuous?",
    	answers: [
    	"Действие, выраженное глаголом в Present Continuous, во всех случаях совершается постоянно или регулярно в настоящем времени.",
    	"Действие совершается всегда в будущем времени.",
    	"Действие совершается в данный момент, или момент речи в настоящем времени.",
    	"Действие уже совершено, и в предложении подчеркивается результат такого действия."
    	],
    	correctAnswer: 2
    },
    {
    	text: "Karina never minds ................. the movie again.",
    	answers: [
    	"to watch",
    	"to be watched",
    	"watch",
    	"watching"
    	],
    	correctAnswer: 3
    },
    {
    	text: "I couldn’t help ................. .",
    	answers: [
    	"for laughing",
    	"and laughed",
    	"laughing",
    	"to laughed"
    	],
    	correctAnswer: 2
    },
    {
    	text: "Можно мне взять Ваш карандаш?",
    	answers: [
    	"Can I take your pencil? ",
    	"Must I take your pencil?",
    	"Should I take your pencil?",
    	"May I take your pencil?"
    	],
    	correctAnswer: 3
    },
    {
    	text: "Марта никогда не слышала, как он говорит по-английски.",
    	answers: [
    	"Martha never heard him spoke English.",
    	"Martha never heard him to speak English.",
    	"Martha has never heard him speak English.",
    	"Martha never heard how he speaks English."
    	],
    	correctAnswer: 2
    },
    {
    	text: "Я знаю его четыре года.",
    	answers: [
    	"I know him four years. ",
    	"I have been knowing him for four years.",
    	"I know him for four years.",
    	"I have known him for four years."
    	],
    	correctAnswer: 3
    },
    {
    	text: "В каком из представленных ниже слов звук, который передаётся буквой «a», отличается от остальных:",
    	answers: [
    	"map",
    	"tape",
    	"age",
    	"make"
    	],
    	correctAnswer: 0
    },
    {
    	text: "I have ................. butter, please, buy some.",
    	answers: [
    	"little",
    	"many",
    	"few",
    	"a few"
    	],
    	correctAnswer: 0
    },
    {
    	text: "The taxi ................. by 7 o’clock yesterday.",
    	answers: [
    	" has arrived",
    	"had arrived",
    	"arrived",
    	"is arrived"
    	],
    	correctAnswer: 1
    },
    {
    	text: "Должно быть, он продал свою машину.",
    	answers: [
    	"It must be that he has sold his car.",
    	"He must sold his car.",
    	"He should have solden his car.",
    	"He must have sold his car."
    	],
    	correctAnswer: 3
    },
    {
    	text: "Я хочу, чтобы погода была хорошая.",
    	answers: [
    	"I want that the weather will be fine.",
    	"I want the weather to be fine.",
    	"I want the weather be fine.",
    	"I want the weather being fine."
    	],
    	correctAnswer: 1
    },
    {
    	text: " Какой же он умный мальчик!",
    	answers: [
    	"What an intelligent boy is he!",
    	"What the intelligent boy is he!",
    	"What an intelligent boy he is!",
    	"What the intelligent boy he is!"
    	],
    	correctAnswer: 2
    },
    {
    	text: " Find the incorrect sentence.",
    	answers: [
    	"Though it was nine o’clock in the evening, there were not many people in the bar.",
    	"Although it is nine o’clock in the evening, there are not many people in the restaurant.",
    	"It was only nine o’clock in the morning, and there were too many people in the café.",
    	"Through it was eight o’clock in the morning, there weren’t many people in the pub."
    	],
    	correctAnswer: 3
    },
    {
    	text: " Какое из перечисленных ниже предложений нельзя перевести на русский язык как «Я читаю»?",
    	answers: [
    	"  I read magazines every day.",
    	"I am reading a book.",
    	"I have been reading the magazine for two hours.",
    	"Все варианты подходят."
    	],
    	correctAnswer: 3
    },
    {
    	text: " When Kate ................. at Pier 90, it was crowded with football fans.",
    	answers: [
    	"achieved",
    	"arrived",
    	"entered",
    	"reached"
    	],
    	correctAnswer: 1
    },
    {
    	text: " There was no one to cheer him ................. .",
    	answers: [
    	"on",
    	"in",
    	"up",
    	"over"
    	],
    	correctAnswer: 2
    },
    {
    	text: " Could you possibly give me ................. ?",
    	answers: [
    	"a advice",
    	"an advice",
    	"some advices",
    	"a piece of advice"
    	],
    	correctAnswer: 3
    },
    {
    	text: " Marvin asked me ................. .",
    	answers: [
    	"        what was my favourite vegetable",
    	"what my favourite vegetable was",
    	"what is my favourite vegetable",
    	"what about my favourite vegetable"
    	],
    	correctAnswer: 1
    },
    {
    	text: " The accident happened ................. our way home.",
    	answers: [
    	"in",
    	"on",
    	"for",
    	"about"
    	],
    	correctAnswer: 1
    },
    {
    	text: " If he were not so absent-minded, he ................. you for your sister (yesterday).",
    	answers: [
    	"would not mistake",
    	"would not have mistaken",
    	"would not have been mistaken",
    	"did not mistake"
    	],
    	correctAnswer: 1
    },
    {
    	text: " If Mike lived in the country house, he ................. happier.",
    	answers: [
    	"was",
    	"is",
    	"will be",
    	"would be"
    	],
    	correctAnswer: 3
    },
    {
    	text: " ................. that wierd man sitting over there?",
    	answers: [
    	"Which",
    	"Whose",
    	"Who's",
    	"Who"
    	],
    	correctAnswer: 2
    },
    {
    	text: " How long ................. his house?",
    	answers: [
    	"has Mr Johnson had",
    	"does Mr Johnson have",
    	"had Mr Johnson had",
    	"has Mr Johnson been having"
    	],
    	correctAnswer: 0
    },
    {
    	text: " Ron has made up his ................. to become a teacher.",
    	answers: [
    	"brains",
    	"decision",
    	"head",
    	"mind"
    	],
    	correctAnswer: 3
    },
    {
    	text: " If Deborah ................. to dinner tomorrow, I'll be happy.",
    	answers: [
    	"  will come",
    	"comes",
    	"came",
    	"was coming"
    	],
    	correctAnswer: 1
    },
    {
    	text: " Ask somebody for ................. occupation.",
    	answers: [
    	"his",
    	"her",
    	"their",
    	"its"
    	],
    	correctAnswer: 2
    },
    {
    	text: " Kids shouldn't take those pills, and ................. .",
    	answers: [
    	"neither should she",
    	"neither she should",
    	"she did either",
    	"either shouldn't she"
    	],
    	correctAnswer: 0
    },
    {
    	text: " The doctor ................. me that there would be no pain.",
    	answers: [
    	"sured",
    	"insured",
    	"reassured",
    	"ensured"
    	],
    	correctAnswer: 2
    },
    {
    	text: "I am looking for an ................. method of heating.",
    	answers: [
    	"economics",
    	"economy",
    	"economic",
    	"economical"
    	],
    	correctAnswer: 3
    },
    {
    	text: " We try to be ................. to the needs of the customer.",
    	answers: [
    	"responsible",
    	"responsive",
    	"respondent",
    	"response"
    	],
    	correctAnswer: 1
    },
    {
    	text: "An obstetrician/gynecologist at the pre-conception clinic suggests we ................. some further tests.",
    	answers: [
    	"doing",
    	"to do",
    	"are doing",
    	"should do"
    	],
    	correctAnswer: 3
    },
    {
    	text: " This particular college has a very selective ................. policy.",
    	answers: [
    	"acceptance",
    	"entrance",
    	"admissions",
    	"admittance"
    	],
    	correctAnswer: 2
    }
    ];

    var yourAns = new Array;
    var score = 0;

    function Engine(question, answer) {yourAns[question]=answer;}

    function Score(){
    	var answerText = "Результаты:\n";
    	for(var i = 0; i < yourAns.length; ++i){
    		var num = i+1;
    		answerText=answerText+"\n    Вопрос №"+ num +"";
    		if(yourAns[i]!=questions[i].correctAnswer){
    			answerText=answerText+"\n    Правильный ответ: " +
    			questions[i].answers[questions[i].correctAnswer] + "\n";
    		}
    		else{
    			answerText=answerText+": Верно! \n";
    			++score;
    		}
    	}

    	answerText=answerText+"\nВсего правильных ответов: "+score+"\n";
		var correct_answers="\nВсего правильных ответов: "+score+"\n";
    	// console.log(answerText);
    	yourAns = [];
    	score = 0;
    	clearForm("quiz");

    	return correct_answers;
    }
    function clearForm(name) {
    	var f = document.forms[name];
    	for(var i = 0; i < f.elements.length; ++i) {
    		if(f.elements[i].checked){
    			// console.log(f.elements[i].value)
   // f.elements[i].checked = false;
}
}
}
</script>

<style>
	span.quest {font-weight: bold;}
</style>

<h1><script>document.write(title)</script></h1>
<h2><script>document.write(subtitle)</script></h2>

<form id="form_quiz" name="quiz">
	<ol>
		<script>
			for(var q=0; q<questions.length; ++q) {
				var question = questions[q];
				var idx = 1 + q;

				document.writeln('<li><span class="quest">' + question.text + '</span><br/>');
				for(var i in question.answers) {
					document.writeln('<label for="' + idx + i + '" ><input id="' + idx + i + '"  type=radio name="q' + idx + '" value="' + i +
						'" onClick="Engine(' + q + ', this.value)">'+question.answers[i]+ '</label><br/>');
				}
				document.writeln('<hr>');
			}
		</script>
	</ol>

<input type="text" name="name" id="name" placeholder='Имя'><br><br>
<input type="tel" name="phone" id="phone" placeholder='Ваш телефон'><br><br>
<input type='email' name="email" id="email" placeholder='Ваш e-mail'><br><br><br>



	<input type="submit" value="Проверить результаты">

</form>
<style>
	li {
		margin-bottom: 10px;
	}
	 ol  li label input {
        -webkit-appearance:normal !important; 
    }
</style>
</div>
</div>
</div>

<?php get_footer(); ?>
