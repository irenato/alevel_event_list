<?php
/**
 * Template name: Project-page
 */

?>

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">

    <title><?= get_the_title(); ?></title>
    <meta name="description" content="">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Template Basic Images Start -->
    <link rel="shortcut icon" href="http://a-level.com.ua/wp-content/themes/alevel/favicon.png" type="image/x-icon">

    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/libs.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/fonts.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/css/main.css">

</head>

<body>
<!-- header -->
<header>
    <div class="container">
        <a href="http://a-level.com.ua"><img src="<?= get_template_directory_uri() ?>/img/logo.png" alt="logo"
                                             class="img-responsive"></a>
        <p class="text"><?= get_field('event_info') ?> </p>
        <?php $_SESSION['title'] = get_the_title() . ' ' . get_field('event_title'); ?>
        <h1><?= get_the_title(); ?></h1>
        <h2><?= get_field('event_title') ?></h2>
        <p class="conf"><?= get_field('event_logan') ?></p>
        <a href="#" class="btn register-modal-call" data-toggle="modal" data-target=".bs-example-modal-lg"
           data-action-name="register-me">Регистрация</a>
    </div>
</header>
<!-- main -->
<main>
    <!-- about -->
    <div class="about_conf">
        <div class="container">
            <h2>О конференции</h2>
            <p class="life">
                <?= get_field('event_about') ?>
            </p>
            <?php $causes = get_field('causes'); ?>
            <h3><?= count($causes) ?> причин<?= count($causes) <= 4 ? 'ы' : '' ?>,
                почему нельзя пропустить:</h3>

            <!-- Lg and md -->
            <div class="row">
                <!-- sm and xs -->
                <div class="row sm_xs clearfix">

                    <?php $i = 0; ?>
                    <?php foreach ($causes as $cause) : ?>
                        <?php $i++; ?>
                        <div
                            class="col-md-<?= ($i >= count($causes) - 1) ? '6' : '4' ?> col-sm-6 col-xs-6 block <?= $i < count($causes) ? '' : 'centr'; ?>" <?= ($i >= count($causes) - 1) ? "style=min-height: 200px" : "" ?>>
                            <img src="<?= $cause['cause_image']["sizes"]['thumbnail'] ?>" alt="img<?= $i; ?>"
                                 class="img-responsive">
                            <p><?= $cause['cause_description'] ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <!-- /about -->
        <!-- program -->
        <div class="program">
            <h2>Программа ивента</h2>
            <div class="container">
                <div class="row">

                    <?php $events = get_field('events'); ?>
                    <?php $i = 0; ?>
                    <?php foreach ($events as $event) : ?>
                        <?php $i++; ?>
                        <?php if ($event['event_cause_name'] && $event['event_cause_logo']) : ?>
                            <div class="col-xs-12 small <?= $i == 0 ? 'regist' : '' ?>">
                                <span class="time"> <?= $event['event_cause_time'] ?></span>
                                <div class="person">
                                    <img src="<?= $event['event_cause_logo'] ?>" alt="photo"
                                         class="photo">
                                    <span class="reg"><?= $event['event_cause_name'] ?></span>
                                </div>
                            </div>
                        <?php elseif ($event['event_speaker_photo'] && $event['event_speaker_description']) : ?>
                            <div class="col-xs-12 big">
                                <span class="time"> <?= $event['event_cause_time'] ?></span>
                                <div class="person">
                                    <div class="speaker-photo">
                                        <img src="<?= $event['event_speaker_photo'] ?>" alt="photo"
                                             class="photo">
                                    </div>
                                    <div class="speaker-info">
                                        <h4><?= $event['event_speaker_name'] ?></h4><br>
                                        <p><?= $event['event_speaker_description'] ?></p><br>
                                        <span><?= $event['event_speaker_course'] ?></span>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
    <!-- program -->
    <div class="bonus">
        <div class="container">
            <h2>Бонусы учасникам</h2>
            <p>в дополнение к актуальным и ценным знаниям</p>
        </div>
    </div>
    <!-- gift -->
    <div class="gift">
        <div class="container">
            <div class="row">
                <div class="col-md-6 first">
                    <img src="<?= get_template_directory_uri() ?>/img/card.png" alt="card" class="pl">
                    <div class="ml">
             <span class="text">В подарок
             на любой курс</span>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="cube">
                        <span>Бесплатный курс за репост</span>
                        <a href="<?= get_field('link_vk_promo') ?>" class="btn vk_btn" target="_blank">Подробнее<i
                                class="fa fa-2x fa-vk" aria-hidden="true"></i></a>
                    </div>
                </div>


                <div class=" package">
                    <div class=" col-xs-12 rel">
                        <img src="<?= get_template_directory_uri() ?>/img/package.png" alt="" class="img-responsive">
                        <span class="text">Сюрпризы от нас
            и наших партнеров</span>
                        <div class="btn register-modal-call" data-toggle="modal" data-target=".bs-example-modal-lg"
                             data-action-name="register-me">Круто, я
                            пойду!
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /gift -->
    <div class="organizer">
        <div class="container">
            <h2>Организатор</h2>
            <div class="row org">
                <div class="col-sm-6">
                    <img src="<?= get_template_directory_uri() ?>/img/logo.png" alt="">
                </div>
                <div class="col-sm-6">
                    <h4>A-level Ukraine</h4>
                    <p><?= get_field('about_us') ?></p>
                    <a href="http://a-level.com.ua/"><span>www.A-level.com.ua</span> <i class="fa fa-external-link"
                                                                                        aria-hidden="true"></i></a>
                </div>
            </div>

            <h2>Партнеры</h2>
            <?php $partners = get_field('partners');
            $count_of_partners = count($partners);
            ?>
            <div class="filtering">
                <?php $i = 0; ?>
                <?php foreach ($partners as $partner) : ?>
                    <?php $i++; ?>
                    <?php
                    if ($partner['partner_link'])
                        $our_partner = "<a href='" . $partner['partner_link'] . "' target='_blank'><img src='" . $partner['partner'] . "' alt=''></a>";
                    else
                        $our_partner = "<img src='" . $partner['partner'] . "' alt=''>"; ?>
                    <?php if ($i <= 8) : ?>
                        <?php if ($i != $count_of_partners && $i % 2 != 0) : ?>
                            <div class="col-md-3 col-xs-4">
                            <?= $our_partner ?>
                        <?php elseif ($i % 2 != 0): ?>
                            <div class="col-md-3 col-xs-4">
                                <?= $our_partner ?>
                            </div>
                        <?php else : ?>
                            <?= $our_partner ?>
                            </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <div class="col-md-3 col-xs-4">
                            <?= $our_partner ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <div class="filtering for-mobile">
                <?php $i = 0; ?>
                <?php foreach ($partners as $partner) : ?>
                    <?php
                    if ($partner['partner_link'])
                        $our_partner = "<a href='" . $partner['partner_link'] . "' target='_blank'><img src='" . $partner['partner'] . "' alt=''></a>";
                    else
                        $our_partner = "<img src='" . $partner['partner'] . "' alt=''>"; ?>
                    <?php $i++; ?>
                    <?php if ($i != $count_of_partners && $i % 2 != 0) : ?>
                        <div class="col-md-3 col-xs-4">
                        <?= $our_partner ?>
                    <?php elseif ($i % 2 != 0) : ?>
                        <div class="col-md-3 col-xs-4">
                            <?php if ($partners[0]['partner_link']) : ?>
                                <a href="<?= $partners[0]['partner_link'] ?>" target="_blank">
                                    <img src="<?= $partners[0]['partner'] ?>" alt="">
                                </a>
                            <?php else : ?>
                                <img src="<?= $partners[0]['partner'] ?>" alt="">
                            <?php endif; ?>
                            <?= $our_partner ?>
                        </div>
                    <?php else : ?>
                        <?= $our_partner ?>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
            <h5>Готовы стать партнером?</h5>
            <div class="btn register-modal-call" data-toggle="modal" data-target=".bs-example-modal-lg"
                 data-action-name="be-a-partner"><a href="#">Свяжитесь с нами</a>
            </div>
        </div>
    </div>
    <!-- /Organizer -->
    <!-- subscribe -->
    <div class="subscribe">
        <div class="container">
            <h5>Есть события, которые нельзя пропустить.</h5>
            <div class="p">подпишись, чтобы быть в курсе!</div>
            <img src="<?= get_template_directory_uri() ?>/img/air.png" alt="" class="img-responsive">
            <div class="btn" data-toggle="modal" data-target=".bs-example-modal-lg1"
                 data-action-name="get-notifications"><a href="#">Подписаться</a></div>
        </div>
    </div>
    <!-- /subscribe -->

    <div class="registration">
        <div class="container">
            <form>

                <input type="text" name="name" placeholder="Имя" required>
                <input type="text" name="second-name" placeholder="Фамилия" required>
                <input type="email" name="email" placeholder="E-mail" required>
                <input type="tel" name="phone" placeholder="Телефон" id="mask">
                <div class="div-experience">
                    <label for="experience">Есть опыт в IT</label>
                    <input class="experience" type="checkbox">
                </div>
                <span id="errors-on-main-form" class="wpcf7-form-control-wrap  text-danger"
                      style="display: none;"></span>
                <button id="main-form" class="btn" data-send="register-me">Зарегистрироваться</button>

            </form>
            <!--            --><? //= do_shortcode('[contact-form-7 id="132" title="Регистрация на мероприятиии"]'); ?>
        </div>

    </div>
    <div id="contact_map">

    </div>
</main>

<footer>
    <div class="container">
        <div class="col-md-4">
            <p class="place">место проведения</p>
            <p class="street"><?= get_field('event_location') ?></p>
        </div>
        <div class="col-md-4">
            <a href="http://a-level.com.ua" class="db_a">
                <img src="<?= get_template_directory_uri() ?>/img/logo.png" alt="logo" class="img-responsive">
            </a>
            <div class="icon">
                <a href="<?= get_field('link_vk') ?>" target="_blank"><i class="fa fa-2x fa-vk" aria-hidden="true"></i></a>
                <!--                    <a><i class="fa fa-2x fa-linkedin" aria-hidden="true"></i></a>-->
                <!--                <a href="-->
                <? //= get_field('link_googleplus') ?><!--" target="_blank"><i class="fa fa-2x fa-google-plus"-->
                <!--                                                                                 aria-hidden="true"></i></a>-->
                <a href="<?= get_field('link_fb') ?>" target="_blank"><i class="fa fa-2x fa-facebook"
                                                                         aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="col-md-4">
            <p class="questions">По вопросам:</p>
            <p><?= get_field('alevel_admin_name'); ?></p>
            <?php $phones = get_field('contact_phones'); ?>
            <?php foreach ($phones as $phone) : ?>
                <a href="tel:<?= $phone['contact_phone'] ?>" class="a_block"><?= $phone['contact_phone'] ?></a>
            <?php endforeach; ?>
            <a href="skype:<?= get_field('skype') ?>?chat" class="a_block">Skype: <?= get_field('skype') ?></a>
        </div>
    </div>
</footer>

<!-- modul registration -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">

        <div class="modal-content subscribe_modal">
            <div class="registration">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <input type="text" name="name" placeholder="Имя" required>
                <input type="text" name="second-name" placeholder="Фамилия" required>
                <input type="email" name="email" placeholder="E-mail" required>
                <input id="mask1" type="tel" name="phone" placeholder="Телефон">
                <div class="div-experience" style="display: none">
                    <label for="experience">Есть опыт в IT</label>
                    <input class="experience" type="checkbox">
                </div>
                <span id="errors-on-next-form" class="wpcf7-form-control-wrap  text-danger"
                      style="display: none;"></span>
                <button id="next-form" class="btn">Зарегистрироваться</button>
            </div>
        </div>
    </div>
</div>

<!-- modul subscribe -->
<div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content subscribe_modal">
            <div class="registration">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <input type="text" name="name" placeholder="Имя" required>
                <input type="email" name="email" placeholder="E-mail" required>
                <span id="errors-on-next-form" class="wpcf7-form-control-wrap  text-danger"
                      style="display: none;"></span>
                <button id="subscribe-form" class="btn">Подписаться</button>
            </div>
        </div>
    </div>
</div>


<!-- modul welcome -->
<div class="modal fade bs-example-modal-lg2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     id="welcome">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="welcome">
                <h5>Добро пожаловать в мир IT!</h5>
                <img src="<?= get_template_directory_uri() ?>/img/mail.png" alt="mail" class="img-responsive">
                <p>Подтвердите свое участие, перейдя по ссылке из письма, которое только что ушло вам на
                    почту.</p>
                <p class="say">Расскажи о конференции друзьям:</p>
                <div class="icon">
                    <a href="<?= get_field('link_vk') ?>" target="_blank"><i class="fa fa-2x fa-vk"
                                                                             aria-hidden="true"></i></a>
                    <!--                    <a><i class="fa fa-2x fa-linkedin" aria-hidden="true"></i></a>-->
                    <!--                    <a href="-->
                    <? //= get_field('link_googleplus') ?><!--" target="_blank"><i class="fa fa-2x fa-google-plus"-->
                    <!--                                                                                     aria-hidden="true"></i></a>-->
                    <a href="<?= get_field('link_fb') ?>" target="_blank"><i class="fa fa-2x fa-facebook"
                                                                             aria-hidden="true"></i></a>
                </div>
                <p class="ps">P.S Если письмо еще не пришло, повторите регистрацию.</p>
            </div>
        </div>
    </div>
</div>
<!-- access welcome -->
<div class="modal fade bs-example-modal-lg3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="success">
                <h5>Теперь ты точно зарегистрирован!</h5>
                <img src="<?= get_template_directory_uri() ?>/img/bird.png" alt="bird" class="img-responsive">
                <p class="say">До встречи. И не забудь рассказать о конференции друзьям:</p>
                <div class="icon">
                    <a href="<?= get_field('link_vk') ?>" target="_blank"><i class="fa fa-2x fa-vk"
                                                                             aria-hidden="true"></i></a>
                    <!--                    <a><i class="fa fa-2x fa-linkedin" aria-hidden="true"></i></a>-->
                    <!--                    <a href="-->
                    <? //= get_field('link_googleplus') ?><!--" target="_blank"><i class="fa fa-2x fa-google-plus"-->
                    <!--                                                                                     aria-hidden="true"></i></a>-->
                    <a href="<?= get_field('link_fb') ?>" target="_blank"><i class="fa fa-2x fa-facebook"
                                                                             aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modul welcome -->
<div class="modal fade bs-example-modal-lg4" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     id="welcome">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="welcome">
                <h5><?= get_option('partners_element2_title') ?></h5>
                <img src="<?= get_template_directory_uri() ?>/img/mail.png" alt="mail" class="img-responsive">
                <p><?= get_option('modal_for_partners2') ?></p>
                <p class="say">Расскажи о конференции друзьям:</p>
                <div class="icon">
                    <a href="<?= get_field('link_vk') ?>" target="_blank"><i class="fa fa-2x fa-vk"
                                                                             aria-hidden="true"></i></a>
                    <!--                    <a><i class="fa fa-2x fa-linkedin" aria-hidden="true"></i></a>-->
                    <!--                    <a href="-->
                    <? //= get_field('link_googleplus') ?><!--" target="_blank"><i class="fa fa-2x fa-google-plus"-->
                    <!--                                                                                     aria-hidden="true"></i></a>-->
                    <a href="<?= get_field('link_fb') ?>" target="_blank"><i class="fa fa-2x fa-facebook"
                                                                             aria-hidden="true"></i></a>
                </div>
                <p class="ps">P.S Если письмо еще не пришло, повторите регистрацию.</p>
            </div>
        </div>
    </div>
</div>
<!-- access welcome -->

<!-- access welcome -->
<div class="modal fade bs-example-modal-lg5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="success">
                <h5><?= get_option('partners_element3_title') ?></h5>
                <img src="<?= get_template_directory_uri() ?>/img/bird.png" alt="bird" class="img-responsive">
                <p class="say"><?= get_option('modal_for_partners3') ?></p>
                <div class="icon">
                    <a href="<?= get_field('link_vk') ?>" target="_blank"><i class="fa fa-2x fa-vk"
                                                                             aria-hidden="true"></i></a>
                    <!--                    <a><i class="fa fa-2x fa-linkedin" aria-hidden="true"></i></a>-->
                    <!--                    <a href="-->
                    <? //= get_field('link_googleplus') ?><!--" target="_blank"><i class="fa fa-2x fa-google-plus"-->
                    <!--                                                                                     aria-hidden="true"></i></a>-->
                    <a href="<?= get_field('link_fb') ?>" target="_blank"><i class="fa fa-2x fa-facebook"
                                                                             aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- new subscriber -->
<div class="modal fade bs-example-modal-lg6" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <div class="success">
                <h5>Спасибо за подписку!</h5>
                <img src="<?= get_template_directory_uri() ?>/img/bird.png" alt="bird" class="img-responsive">

                <div class="icon">
                    <a href="<?= get_field('link_vk') ?>" target="_blank"><i class="fa fa-2x fa-vk"
                                                                             aria-hidden="true"></i></a>
                    <a href="<?= get_field('link_fb') ?>" target="_blank"><i class="fa fa-2x fa-facebook"
                                                                             aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?= get_template_directory_uri() ?>/js/libs.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script type="text/javascript" src="<?= get_template_directory_uri() ?>/js/jquery.maskedinput-1.3.js"></script>
<script
    src='https://maps.googleapis.com/maps/api/js?key=AIzaSyDoFR4O2S0D5Uebjtxa9hCQ3bx2oiIhZz4&amp;libraries=places'></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script src="<?= get_template_directory_uri() ?>/js/main.js"></script>
<script src="<?= get_template_directory_uri() ?>/js/action.js"></script>
<?php if (isset($_GET['path']) && confirm_registration($_GET['path'], $_GET['action'])) :
//<?php if (isset($_GET['path']) && isset($_SESSION[$_GET['path']])) :
    if ($_GET['action'] == 'partners') :
        ?>
        <script src="<?= get_template_directory_uri() ?>/js/reg_partner.js"></script>
    <?php else : ?>
        <script src="<?= get_template_directory_uri() ?>/js/reg.js"></script>
        <?php
    endif;
    if (final_of_registration($_SESSION[$_GET['path']]))
        unset($_SESSION[$_GET['path']]);
endif;
?>

</body>
</html>

