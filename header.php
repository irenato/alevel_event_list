<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />

  <title>    
    <?php
    wp_title( '|', true, 'right' );
    ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" id="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no"/>
  </head>
  <body>
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NT9299"
      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NT9299');</script>
    <!-- End Google Tag Manager -->
    <script>
      function loader(_success) {
        var obj     = document.querySelector('.preloader'),
        line    = document.querySelector('.preloader_line'),
        num     = document.querySelector('.preloader_num');
        var w = 0,
        t = setInterval(function () {
          w = w + 1;
          line.style.width = w + '%';
          num.textContent = w + '%';
          if (w === 100) {
            obj.style.display = 'none';
            clearInterval(t);
            w = 0;
            if (_success) {
              return;
            }
          }
        }, 20);
      }
      window.onload = loader;
    </script>
    <style type="text/css">    
      .preloader {
        background: #fff;
        position: fixed;
        z-index: 99999;
        top: 0;
        left: 0;
        width: 100vw;
        height: 100vh;
        -webkit-transition: .5s;
        transition: .5s;
      }
      
      .preloader img {
        display: block;
        position: absolute;
        top: 30%;
        left: 50%;
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%);
      }
      
      .preloader_num {
        display: block;
        font-size: 20px;
        position: absolute;
        top: 48%;
        left: 50%;
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%);
      }
      
      .preloader_line {
        display: block;
        background: #64c1cc;
        height: 2px;
        width: 0;
        position: absolute;
        top: 56%;
        left: 50%;
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%);
      }
    </style>
    <div class="preloader">
      <img src="<?=get_template_directory_uri()?>/img/logo.png" alt="Preloader image">
      <div class="preloader_num">0%</div>
      <div class="preloader_line"></div>
    </div>
    <?php wp_head();?>
    <header <?php  echo (is_page_template('view_templates/main.php'))? : 'class="topHeader"';?>>
      <div class="logo"><a href="<?=home_url();?>"><img src="<?=get_template_directory_uri()?>/img/logo.png" alt="Logo"></a></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-11 col-lg-offset-1 navigation">
            <nav>
              <ul>
                <li>
                  <a href="<?php echo (is_page_template('view_templates/main.php')) ? '#aboutUs' : home_url().'/#aboutUs'?>">о нас</a>
                  <span></span>
                </li>
                <li>
                  <a href="<?php echo home_url().'/?page_id=42'?>">курсы</a>
                  <span></span>
                  <ul class="sub-menu">
                    <?php 
                    $args=array('post_type'=>'courses');
                    $query = new wp_Query($args);
                    while ($query->have_posts()) {
                      $query->the_post();
                      $single_post_id = get_the_ID();
                      $name_cours = get_field('name_cours', $single_post_id);
                      echo '<li><a href="'.get_the_permalink().'">'.$name_cours.'</a><span></span></li>';
                    } ?>
                  </ul>
                </li>
                <?php $count_tidings = wp_count_posts('tidings');   ?>
                <?php if($count_tidings->publish!=0):?>
                  <li>
                    <a href="<?php echo (is_page_template('view_templates/main.php')) ? '#news' : home_url().'/#news'?>">новости</a>
                    <span></span>
                  </li>
                <?php endif;?>
                <?php $count_partners = wp_count_posts('partners'); ?>
                <?php if($count_partners->publish!=0):?>
                  <li>
                    <a href="<?php echo (is_page_template('view_templates/main.php')) ? '#partners' : home_url().'/#partners'?>">партнеры</a>
                    <span></span>
                  </li>
                <?php endif;?>
                <?php $count_reviews = wp_count_posts('reviews');?>
                <?php if($count_reviews->publish!=0):?>
                  <li>
                    <a href="<?php echo (is_page_template('view_templates/main.php')) ? '#reviews' : home_url().'/#reviews'?>">отзывы</a>
                    <span></span>
                  </li>
                <?php endif;?>
              </ul>
            </nav>
            <div class="tel">
              <ul>
               <?php
               if ( function_exists('dynamic_sidebar') )
                dynamic_sidebar('tel-main');
              ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="enroll"><a href="#enroll_course">Записаться на курс</a></div>
  </header>