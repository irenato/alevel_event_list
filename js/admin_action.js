$(document).ready(function () {
    $('body').on('click', 'a.confirm-it-now', function (e) {
        e.preventDefault();
        var app_id = $(this).attr('data-id'),
            send_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'confirmApplication',
                    'id': app_id,
                },
            });
        send_data.done(function(){
            location.reload();
        })
    });
    
    $('body').on('click', 'a.delete-now', function (e) {
        e.preventDefault();
        var app_id = $(this).attr('data-id'),
            send_data = $.ajax({
                url: '/wp-admin/admin-ajax.php',
                type: 'POST',
                data: {
                    'action': 'deleteApplication',
                    'id': app_id,
                },
            });
        send_data.done(function(){
            location.reload();
        })
    });

})
