<?php
function theme_settings_page()
{

    echo '<div class="wrap">';
    echo '<h1>Настройки темы</h1>';
    echo '<form method="post" action="options.php" enctype="multipart/form-data">';
    settings_fields("section");
    do_settings_sections("theme-options");
    submit_button();
    echo '</form>';
    echo '</div>';
}

function display_email_for_guests_element()
{
    echo '<input type="email" name="email_for_guests" id="email_for_guests" value="' . get_option('email_for_guests') . '"/>';
}

function display_email_for_partners_element()
{
    echo '<input type="email" name="email_for_partners" id="email_for_partners" value="' . get_option('email_for_partners') . '"/>';
}

function display_email_for_notifications_element()
{
    echo '<input type="email" name="email_for_notifications" id="email_for_notifications" value="' . get_option('email_for_notifications') . '"/>';
}

function display_email_from_element()
{
    echo '<input type="text" name="field_from" id="field_from" value="' . get_option('field_from') . '"/>';
}

function display_confirm_email_link_element()
{
    echo '<input type="text" name="confirm_email_link" id="confirm_email_link" value="' . get_option('confirm_email_link') . '"/>';
}

function display_confirm_email_text_element()
{
    echo '<textarea name="confirm_email_text" id="confirm_email_text" rows="10" cols="30">' . get_option('confirm_email_text') . '</textarea>';
}

function display_confirm_email_text_element_partners()
{
    echo '<textarea name="confirm_email_text_partners" id="confirm_email_text_partners" rows="10" cols="30">' . get_option('confirm_email_text_partners') . '</textarea>';
}

function display_confirm_email_subject_element()
{
    echo '<textarea name="confirm_email_subject" id="confirm_email_subject" rows="10" cols="30">' . get_option('confirm_email_subject') . '</textarea>';
}

function display_modal_for_partners_element2()
{
    echo '<textarea name="modal_for_partners2" id="modal_for_partners2" rows="10" cols="30">' . get_option('modal_for_partners2') . '</textarea>';
}

function display_modal_for_partners_element2_title()
{
    echo '<input type="text" name="partners_element2_title" id="partners_element2_title" value="' . get_option('partners_element2_title') . '"/>';
}

function display_modal_for_partners_element3()
{
    echo '<textarea name="modal_for_partners3" id="modal_for_partners3" rows="10" cols="30">' . get_option('modal_for_partners3') . '</textarea>';
}

function display_modal_for_partners_element3_title()
{
    echo '<input type="text" name="partners_element3_title" id="partners_element3_title" value="' . get_option('partners_element3_title') . '"/>';
}

function display_theme_panel_fields()
{
    add_settings_section("section", "", null, "theme-options");
    add_settings_field("email_for_guests", "E-mail для участников мероприятия", "display_email_for_guests_element", "theme-options", "section");
    add_settings_field("email_for_partners", "E-mail для партнеров", "display_email_for_partners_element", "theme-options", "section");
    add_settings_field("email_for_notifications", "E-mail для подписчиков", "display_email_for_notifications_element", "theme-options", "section");
    add_settings_field("field_from", "Отправитель(from)", "display_email_from_element", "theme-options", "section");
    add_settings_field("confirm_email_subject", "Тема письма для подтверждения почты", "display_confirm_email_subject_element", "theme-options", "section");
    add_settings_field("confirm_email_text", "Текст для подтверждения email", "display_confirm_email_text_element", "theme-options", "section");
    add_settings_field("confirm_email_text_partners", "Текст для подтверждения email (для партнеров)", "display_confirm_email_text_element_partners", "theme-options", "section");
    add_settings_field("confirm_email_link", "Текст ссылки для подтверждения почты", "display_confirm_email_link_element", "theme-options", "section");
    add_settings_field("partners_element2_title", "Заголовок в модалку №2 для партнеров", "display_modal_for_partners_element2_title", "theme-options", "section");
    add_settings_field("modal_for_partners2", "Текст в модалку №2 для партнеров", "display_modal_for_partners_element2", "theme-options", "section");
    add_settings_field("partners_element3_title", "Заголовок в модалку №3 для партнеров", "display_modal_for_partners_element3_title", "theme-options", "section");
    add_settings_field("modal_for_partners3", "Текст в модалку №3 для партнеров", "display_modal_for_partners_element3", "theme-options", "section");


    register_setting("section", "email_for_guests");
    register_setting("section", "email_for_partners");
    register_setting("section", "email_for_notifications");
    register_setting("section", "field_from");
    register_setting("section", "confirm_email_subject");
    register_setting("section", "confirm_email_text");
    register_setting("section", "confirm_email_text_partners");
    register_setting("section", "confirm_email_link");
    register_setting("section", "partners_element2_title");
    register_setting("section", "modal_for_partners2");
    register_setting("section", "partners_element3_title");
    register_setting("section", "modal_for_partners3");

}

add_action("admin_init", "display_theme_panel_fields");

function add_theme_menu_item()
{
    add_menu_page("Информация для рассылки", "Информация для рассылки", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

function add_theme_menu_advers()
{
    add_menu_page("Все заявки", "Заявки", "manage_options", "theme-panel2", "selectAllApplications", 'dashicons-carrot', 4);
}


add_action("admin_menu", "add_theme_menu_advers");

function countNewApplications(){
    global $wpdb;
    $count_applications = $wpdb->get_var("SELECT COUNT(id) FROM wp_event_applications WHERE status=0;");
    if($count_applications)
        return $count_applications;
    else
        return 0;
}

function register_my_custom_submenu_page() {
    add_submenu_page( 'theme-panel2', 'Новые заявки', 'Новые заявки ('.countNewApplications() . ')', 'manage_options', 'my-custom-submenu-page1', 'selectNewApplications' );
}

add_action('admin_menu', 'register_my_custom_submenu_page');

function register_my_custom_submenu_page2() {
    add_submenu_page( 'theme-panel2', 'Обработанные заявки', 'Обработанные заявки', 'manage_options', 'my-custom-submenu-page2', 'selectConfirmedApplications' );
}

add_action('admin_menu', 'register_my_custom_submenu_page2');