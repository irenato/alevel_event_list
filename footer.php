<footer>
    <div id="contact_map"></div>
    <div class="info">
        <?php
        if (function_exists('dynamic_sidebar'))
            dynamic_sidebar('contacts-footer');
        ?>
    </div>
</footer>


<div class="writeReview">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 writeReviewForm">
                <div class="closeModal"><img src="<?= get_template_directory_uri() ?>/img/close.png"></div>
                <div class="double_title">
                    <h3 data-title='Написать отзыв'>Написать отзыв</h3>
                </div>
                <?php echo do_shortcode('[contact-form-7 id="41" title="Написать отзыв"]'); ?>
            </div>
        </div>
    </div>
</div>
<?php wp_footer(); ?>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-77235759-1', 'auto');
    ga('send', 'pageview');

</script>

</body>
</html>
<script>
    function b() {
        var a = document.createElement("link");
        a.rel = "stylesheet";
        a.href = "<?php echo get_template_directory_uri();?>/css/all_css.css";
        document.getElementsByTagName("html")[0].appendChild(a)
    }
    window.addEventListener("load", b);

    function b1() {
        var a = document.createElement("link");
        a.rel = "stylesheet";
        a.href = "<?php echo plugins_url();?>/contact-form-7/includes/css/styles.css";
        document.getElementsByTagName("html")[0].appendChild(a)
    }
    window.addEventListener("load", b1);
</script>

