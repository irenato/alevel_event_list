<?php

function selectAllApplications()
{
    global $wpdb;
    $applications = $wpdb->get_results("SELECT `id`, `username`, `user_second_name`, `eventname`  , `phone`, `email`, `level`, `date`, `status` FROM wp_event_applications ORDER BY `id` DESC ");
    ?>

    <table class="wp-list-table widefat fixed striped pages">
        <tr>
            <th class="manage-column column-author">
                id
            </th>
            <th class="manage-column">
                фамилия
            </th>
            <th class="manage-column">
                имя
            </th>
            <th class="manage-column">
                телефон
            </th>
            <th class="manage-column">
                email
            </th>
            <th class="manage-column">
                название мероприятия
            </th>
            <th class="manage-column">
                уровень участника
            </th>
            <th class="manage-column">
                дата
            </th>
            <th class="manage-column">
                статус заявки
            </th>
        </tr>
        <?php if ($applications): ?>
            <?php foreach ($applications as $application) : ?>
                <tr>
                    <td class="manage-column column-author">
                        <?= $application->id; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->user_second_name; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->username; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->phone; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->email; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->eventname; ?>
                    </td>
                    <td class="manage-column">
                        <?= $application->level == 0 ? 'новичок' : 'есть опыт'; ?>
                    </td>
                    <td class="manage-column">
                        <?= date('d-m-Y H:i:s', $application->date); ?>
                    </td>
                    <td class="manage-column">
                        <?php if ($application->status == 0) : ?>
                            новый
                        <?php elseif ($application->status == 1) : ?>
                            приглашенный
                        <?php else : ?>
                            email не подтверден
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
    </table>

    <?php
}