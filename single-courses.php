<?php
/**
 * Template name: Single-courses page
 */
?>
<?php get_header();?>
<?php 
$queried_object = get_queried_object();
$single_post_id = $queried_object->ID;


$name_cours = get_field('name_cours', $single_post_id);
$diff_cours = get_field('diff_cours', $single_post_id);
$m_some_cours = get_field('m_some_cours', $single_post_id);
$all_descript_cours = get_field('all_descript_cours', $single_post_id);
$meter_start_cours = get_field('meter_start_cours', $single_post_id);
$cost_cours = get_field('cost_cours', $single_post_id);
$duration_cours = get_field('duration_cours', $single_post_id);
$start_cours = get_field('start_cours', $single_post_id);
$weekly_cours = get_field('weekly_cours', $single_post_id);
$program_сours = get_field('program_сours', $single_post_id);
$teachers_cours = get_field('teachers_cours', $single_post_id);
$m_video_cours = get_field('m_video_cours', $single_post_id);

?>
<!-- aboutCourse -->
<div class="aboutCourse">
	<div class="container">
		<div class="row">
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
				<span class="corse">курс</span>
				<div class="double_title">
					<?php  if (!empty($name_cours)):?>
						<h3 data-title='<?=$name_cours?>'><?=$name_cours?>></h3>
					<?php endif; ?>
				</div>
				<div class="level">
					<span>Cложность: </span>
					<?php 
					if(!empty($diff_cours)):
						for($i=0;$i<5;$i++){
							if($i<(integer)$diff_cours){
								echo '<i class="polyFill"></i>';
							}else{
								echo '<i class="polyEmpty"></i>';
							}
						}
						endif;
						?>
					</div>
					<div class="courseFor">
						<span>Для кого курс: </span>
						<ul>
							<?php
							if(!empty($m_some_cours)):
								for($i=0;$i<count($m_some_cours);$i++){
									echo '<li>'.$m_some_cours[$i]['some_cours'].'</li>';
								}
								endif;
								?>

							</ul>
						</div>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
						<?php if(!empty($all_descript_cours)): echo $all_descript_cours; endif; ?>
					</div>
				</div>
				<?php if(!empty($meter_start_cours)):?>
					<div class='timerCourse' title="До старта курса осталось: " data-timer="<?=$meter_start_cours?>"></div>
				<?php endif;?>
			</div>
		</div>
		<!-- aboutCourse -->

		<!-- priceCourse -->
		<div class="priceCourse">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="item">
							<div class="cost">Стоимость:<span><?php if(!empty($cost_cours)): echo $cost_cours; endif; ?></span></div>
							<div class="duration">Продолжительность:<span><?php if(!empty($duration_cours)): echo $duration_cours; endif; ?></span></div>
							<div class="start">Старт:<span><?php if(!empty($start_cours)): echo $start_cours; endif; ?></span></div>
							<div class="week">В неделю:<span><?php if(!empty($weekly_cours)): echo $weekly_cours; endif; ?></span></div>
							<a href="#enroll_course" class="coursEnter">Записаться на курс</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- priceCourse -->

		<!-- coursesProgramm -->
		<?php if(!empty($program_сours)):?>
			<div class="coursesProgramm">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 double_title">
							<h3 data-title='Программа курса'>Программа курса</h3>
						</div>
					</div>

					<div class="row">
						<?php 
						if(!empty($program_сours)):
							$counter=0;
							for($i=0; $i<count($program_сours); $i++){
							if($counter==3){
								$counter=0;
							}?>
						<div class="items col-lg-4 col-md-4 col-sm-6">
							<div class="titleList"><?php if(!empty($program_сours[$i]['duration_part_cours'])): echo $program_сours[$i]['duration_part_cours']; endif; ?></div>
							<div class="line"></div>
							<?php if(!empty($program_сours[$i]['descript_part_cours'])): echo $program_сours[$i]['descript_part_cours']; endif; ?>					
						</div>
						<?php 
						if($counter==1){
							echo '<div class="visible-sm"></div>';
						}
							if($counter==2){
								echo '<div class="clearfix visible-lg visible-md"></div>';
							}
							$counter++;
						?>
						<?php }
						endif;
						?>
						
					</div>
				</div>
			</div>
		<?php endif;?>
		<!-- coursesProgramm -->
		<!-- teachers -->
		<?php if(!empty($teachers_cours)):?>		
			<div class="teachers">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 double_title">
							<h3 data-title='преподаватели'>преподаватели</h3>
						</div>
					</div>
					<div class="row">
						<?php 
						for($i=0; $i<count($teachers_cours); $i++){?>
						<div class="col-lg-4 col-md-4 col-sm-6">
							<div class="item" style="background-image: url(<?php if(!empty($teachers_cours[$i]['foto_teach'])): echo $teachers_cours[$i]['foto_teach']; endif; ?>)">
								<div class="title">
									<h5><?php if(!empty($teachers_cours[$i]['name_tach'])): echo $teachers_cours[$i]['name_tach']; endif; ?></h5>
									<p><?php if(!empty($teachers_cours[$i]['skill_teach'])): echo $teachers_cours[$i]['skill_teach']; endif; ?></p>
								</div>
								<div class="aboutTeacher">
									<div class="facts">Факты о преподавателе:</div>
									<?php if(!empty($teachers_cours[$i]['descript_teach'])): echo $teachers_cours[$i]['descript_teach']; endif; ?>
						<!-- <ul>
							<li>1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. </li>
							<li>2. Integer vel vestibulum erat, id eleifend diam. </li>
							<li>3. Aenean finibus blandit mi, sed vestibulum ante fermentum vitae. </li>
						</ul> -->
					</div>
				</div>
			</div>
			<?php }
			?>
			<div class="clearfix visible-lg visivle-md"></div>
		</div>
	</div>
</div>
<?php endif;?>
<!-- teachers -->

<!-- coursesVideo -->
<?php if(!empty($m_video_cours)):?>
	<div class="coursesVideo">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 double_title">
					<h3 data-title='ВИДЕО С ЗАНЯТИЙ'>ВИДЕО С ЗАНЯТИЙ</h3>
				</div>
			</div>

			<div class="row">
				<?php
				for($i=0; $i<count($m_video_cours); $i++){?>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<div class="item">
						<div class="videoDate">
							<?php if(!empty($m_video_cours[$i]['dat_video'])): echo $m_video_cours[$i]['dat_video']; endif; ?>
						</div>
						<div class="videoItem">
							<iframe width="560" height="315" src="<?php if(!empty($m_video_cours[$i]['link_video'])): echo $m_video_cours[$i]['link_video']; endif; ?>" frameborder="0" allowfullscreen></iframe>
						</div>
						
						<?php if(!empty($m_video_cours[$i]['deccript_video'])): echo $m_video_cours[$i]['deccript_video']; endif; ?>

					</div>
				</div>

				<?php }?>
			</div>
		</div>
	</div>
<?php endif;?>
<!-- coursesVideo -->


<?php $count_reviews = wp_count_posts('reviews');?>
<?php if($count_reviews->publish!=0):?>
	<div class="reviews" id='reviews'>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 double_title">
					<h3 data-title='отзывы'>отзывы</h3>
				</div>
			</div>

			<div class="row">
				<?php 
				$args=array('post_type'=>'reviews');
				$query = new wp_Query($args);
				while ($query->have_posts()) {
					$query->the_post();
					$single_post_id = get_the_ID();

					$name_rev = get_field('name_rev', $single_post_id);
					$url_rev = get_field('url_rev', $single_post_id);
					$cours_rev= get_field('cours_rev', $single_post_id);
					$text_rev = get_field('text_rev', $single_post_id);
					$img_rev = get_field('img_rev', $single_post_id);
					?>

					<div class="col-lg-6 col-md-6 col-sm-6 item">

						<?php  if (!empty($img_rev)):?>
							<span class="reviewImage"><img src="<?=$img_rev?>" alt=""></span>
						<?php endif; ?>
						<div class="course_name">
							<?php  if (!empty($name_rev)):?>
								<div class="name"><?= $name_rev?></div>
								<br>
							<?php endif; ?>
							<?php  if (!empty($name_rev) && !empty($url_rev)):?>
								<a href="<?=$url_rev?>"><?=$cours_rev?></a>
							<?php endif; ?>
						</div>
						<?php  if (!empty($text_rev)):?>
							<?=$text_rev?>
						<?php endif; ?>
					</div>
					<?php } ?>
				</div>
				<div class="row ">
					<a class='write_review' href="">написать отзыв</a>
				</div>
			</div>
		</div>
	<?php endif;?>

	<div class="enroll_course" id='enroll_course'>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 double_title">
					<h3 data-title='записаться на курс'>записаться на курс</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
					<?php echo do_shortcode('[contact-form-7 id="40" title="Записаться на курс"]'); ?>
				</div>
			</div>
		</div>
	</div>


	<?php get_footer();?>