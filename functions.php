<?php

require_once 'lib/enqueue_styles_scripts.php';
require_once 'lib/post_typs.php';
require_once 'lib/ajax_massages.php';
require_once 'lib/admin_page_for_event_list.php';
require_once 'lib/event_all_applications_admin.php';
require_once 'lib/event_new_applications.php';
require_once 'lib/event_confirmed_applications.php';

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'feed_links_extra', 3);

add_theme_support('post-thumbnails');

function theme_register_nav_menu()
{

    register_nav_menus(array(
        'primary' => 'Primary Menu'
    ));
}

add_action('after_setup_theme', 'theme_register_nav_menu');


function register_tel_main()
{
    register_sidebar(array(
        'name' => 'Telefons main',
        'id' => 'tel-main',
        'description' => 'Display contacts on the home page',
        'before_widget' => '<ul>',
        'after_widget' => '</ul>',
    ));
}

add_action('widgets_init', 'register_tel_main');


function register_contacts_footer()
{
    register_sidebar(array(
        'name' => 'Contacts footer',
        'id' => 'contacts-footer',
        'description' => 'Display contacts on the footer',
        'before_widget' => '',
        'after_widget' => '',
    ));
}

add_action('widgets_init', 'register_contacts_footer');


add_action('wp_print_styles', 'remove_style', 100);
function remove_style()
{
    wp_deregister_style('contact-form-7');
}


function add_async_attribute($tag)
{

    return str_replace('src', 'defer  src', $tag);

}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

add_action('init', 'myStartSession', 1);
function myStartSession()
{
    if (!session_id()) {
        session_start();
    }
}

function confirm_registration($data, $action)
{
    global $wpdb;
    $hash = stripcslashes($data);
    if ($action == 'partners')
        $table = 'wp_event_partners';
    else
        $table = 'wp_event_applications';
    $field['status'] = 0;
    $where['hash'] = $hash;
    return $wpdb->update($table, $field, $where);
    die();
}

function updateUserData($hash, $action)
{
    global $wpdb;
    if ($action == 'welcome') {
        $applications = $wpdb->get_row("SELECT * FROM wp_event_applications WHERE `hash`= " . $hash . " ORDER BY `id` DESC ");
        echo "Проверка";
        var_dump($applications);
        die();
        $headers = 'From: ' . $applications[0]->email . '.' . '<' . $applications[0]->email . '>';
//        $to = get_option('email_for_guests');
        $to = 'renato@i.ua';
        $mail_subject = 'Новая заявка на участие в мероприятии ' . "\r\n";
        $mail_text = 'Контактные данные ' . $applications[0]->user_second_name . '' . $applications[0]->username . "\r\n";
        $mail_text .= 'Номер телефона: ' . $applications[0]->phone . "\r\n";
        $mail_text .= 'Электронная почта: ' . $applications[0]->email . "\r\n";

    } else {
        $applications = $wpdb->get_results("SELECT * FROM wp_event_partners WHERE `hash`=" . $hash . " ORDER BY `id` DESC ");
        $to = get_option('email_for_notifications');
        $headers = 'From: ' . $applications[0]->email . '.' . '<' . $applications[0]->email . '>';
        $mail_subject = 'Новый партнер ' . $applications[0]->username . "\r\n";
        $mail_text = 'Контактные данные ' . $applications[0]->user_second_name . '' . $applications[0]->username . "\r\n";
        $mail_text .= 'Номер телефона: ' . $applications[0]->phone . "\r\n";
        $mail_text .= 'Электронная почта: ' . $applications[0]->username . "\r\n";
    }

    return (wp_mail($to, $mail_subject, $mail_text, $headers));
}

function final_of_registration($data)
{
    global $wpdb;
//    $headers = 'MIME-Version: 1.0' . "\r\n";
//    $headers .= 'Content-type: text/html;' . "\r\n";
    $headers = 'From: ' . $data['email'] . '.' . '<' . $data['email'] . '>';
    if ($data['action'] == 'register-me') {
        $to = get_option('email_for_guests');
        $mail_subject = 'Новая заявка на участие в мероприятии ' . "\r\n";
        $mail_text = 'Контактные данные ' . $data['user_second_name'] . ' ' . $data['username'] . "\r\n";
        $mail_text .= 'Номер телефона: ' . $data['phone'] . "\r\n";
        $mail_text .= 'Электронная почта: ' . $data['email'] . "\r\n";

    } else {
        $to = get_option('email_for_notifications');
        $mail_subject = 'Новый партнер ' . $data['name'] . "\r\n";
        $mail_text = 'Контактные данные ' . $data['user_second_name'] . ' ' . $data['username'] . "\r\n";
        $mail_text .= 'Номер телефона: ' . $data['phone'] . "\r\n";
        $mail_text .= 'Электронная почта: ' . $data['email'] . "\r\n";
    }

    return (wp_mail($to, $mail_subject, $mail_text, $headers));
}





