<?php
if (defined('DOING_AJAX') && DOING_AJAX) {
    add_action('wp_ajax_mess_test', 'mess_test');
    add_action('wp_ajax_nopriv_mess_test', 'mess_test');
}
function mess_test()
{
    $mess = $_POST['ans_str'];
    $user_name = $_POST['user_name'];
    $user_phone = $_POST['user_phone'];
    $user_email = $_POST['user_email'];

    $to = 'a.level.ua@gmail.com';
    $subject = 'English test';


    // $headers .= "Reply-To: No-Reply \r\n";
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html;' . "\r\n";
//	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= 'From: a-level <a.level.ua@gmail.com>' . "\r\n";

    $message = iconv("utf-8", "windows-1251", '
	<html>
	<body>
		<img src="' . get_template_directory_uri() . '/img/logo.png" alt="logo">
		<p>' . $user_name . '</p>
		<p>' . $user_phone . '</p>
		<p>' . $user_email . '</p>
		<p>' . $mess . '</p>
	</body>
	</html>
	');

    mail($to, $subject, $message, $headers);

    echo $mess;


    wp_die();
}

add_action('wp_ajax_nopriv_registration_to_event', 'registration_to_event');
add_action('wp_ajax_registration_to_event', 'registration_to_event');
function registration_to_event()
{
    global $wpdb;
    $name = stripcslashes(trim($_POST['user_name']));
    $user_second_name = stripcslashes(trim($_POST['user_second_name']));
    $phone = stripcslashes(trim($_POST['user_phone']));
    $email = stripcslashes(trim($_POST['user_email']));
    $action_name = stripcslashes(trim($_POST['action_name']));
    $to = $email;
    $headers = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html;' . "\r\n";
    $headers .= 'From: ' . get_option('field_from') . '.' . '<a.level.ua@gmail.com>';
    $mail_subject = get_option('confirm_email_subject');
    $hash = gen(7);
    $_SESSION[$hash]['username'] = $name;
    $_SESSION[$hash]['user_second_name'] = $user_second_name;
    $_SESSION[$hash]['phone'] = $phone;
    $_SESSION[$hash]['email'] = $email;
    $_SESSION[$hash]['action'] = $action_name;
    if (isset($_POST['experience']))
        $_SESSION[$hash]['experience'] = $_POST['experience'];
    if ($action_name == 'be-a-partner') {
        $action = 'partners';
        $mail_text = get_option('confirm_email_text_partners') . ": <a href='http://a-level.com.ua/project-lifecycle?path=" . $hash . '&action=' . $action . "'>" . get_option('confirm_email_link') . "</a>";
        $table = 'wp_event_partners';
        $fields['username'] = $name;
        $fields['user_second_name'] = $user_second_name;
        $fields['email'] = strtolower($email);
        $fields['phone'] = $phone;
        $fields['eventname'] = $_SESSION['title'];
        $fields['date'] = time();
        $fields['hash'] = $hash;
        $fields['status'] = 2;
        $wpdb->insert($table, $fields, '');
        if (wp_mail($to, $mail_subject, $mail_text, $headers)) {
            print('done!');
            die();
        } else {
            print('error!');
            die();
        }
    } else {
        $action = 'welcome';
        $mail_text = get_option('confirm_email_text') . ": <a href='http://a-level.com.ua/project-lifecycle?path=" . $hash . '&action=' . $action . "'>" . get_option('confirm_email_link') . "</a>";
        $table = 'wp_event_applications';
        $fields['username'] = $name;
        $fields['user_second_name'] = $user_second_name;
        $fields['email'] = strtolower($email);
        $fields['phone'] = $phone;
        $fields['eventname'] = $_SESSION['title'];
        $fields['level'] = $_POST['experience'];
        $fields['date'] = time();
        $fields['hash'] = $hash;
        $fields['status'] = 2;
        $wpdb->insert($table, $fields, '');
        if (wp_mail($to, $mail_subject, $mail_text, $headers)) {
            print('done!');
            die();
        } else {
            print('error!');
            die();
        }
    }

}

add_action('wp_ajax_nopriv_new_subscriber', 'new_subscriber');
add_action('wp_ajax_new_subscriber', 'new_subscriber');
function new_subscriber()
{
    $name = stripcslashes(trim($_POST['user_name']));
    $email = stripcslashes(trim($_POST['user_email']));
    $headers = 'From: ' . $email . '.' . '<' . $email . '>';
    $to = get_option('email_for_notifications');
    $mail_subject = 'Новая подписка на новости ' . $name . "\r\n";
    $mail_text = 'Контактные данные ' . $name . "\r\n";
    $mail_text .= 'Электронная почта: ' . $email . "\r\n";

    if (wp_mail($to, $mail_subject, $mail_text, $headers)) {
        print('done!');
        die();
    } else {
        print('error!');
        die();
    }

}

function gen($leight)
{
    $x = '';

    $str = "qwertyuiopasdfghjklzxcvbnm123456789";

    for ($i = 0; $i < $leight; $i++) {
        $x .= substr($str, mt_rand(0, strlen($str) - 1), 1);
    }

    return $x;
}

add_action('wp_ajax_nopriv_confirmApplication', 'confirmApplication');
add_action('wp_ajax_confirmApplication', 'confirmApplication');

function confirmApplication()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_event_applications';
    $field['status'] = 1;
    $where['id'] = $id;
    return $wpdb->update($table, $field, $where);
    die();
}

add_action('wp_ajax_nopriv_deleteApplication', 'deleteApplication');
add_action('wp_ajax_deleteApplication', 'deleteApplication');

function deleteApplication()
{
    global $wpdb;
    $id = stripcslashes(trim($_POST['id']));
    $table = 'wp_event_applications';
    $where['id'] = $id;
    return $wpdb->delete($table, $where);
    die();
}

