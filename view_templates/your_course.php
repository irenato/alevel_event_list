<?php
/**
 * Template name: Your_course page
 */
?>

<?php get_header();?>

<div class="courses" id='courses'>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 double_title">
				<h3 data-title='наши курсы'>наши курсы</h3>
			</div>
		</div>
		<div class="row">
			<?php 
			$counter=0;
			$args=array('post_type'=>'courses');
			$query = new wp_Query($args);
			while ($query->have_posts()) {
				$query->the_post();
				$single_post_id = get_the_ID();

				$name_cours = get_field('name_cours', $single_post_id);
				$descript_cours = get_field('descript_cours', $single_post_id);
				$seats_cours = get_field('seats_cours', $single_post_id);
				$start_cours = get_field('start_cours', $single_post_id);
				if($counter==3){
					$counter=0;
				}
				?>
				<div class="col-lg-4 col-md-4 col-sm-6">
					<div class="item item_1">
						<div class="course_name">курс</div>
						<?php  if (!empty($name_cours)):?>
							<h4 class="title"><?=$name_cours?></h4>
						<?php endif; ?>
						<?php  if (!empty($descript_cours)):?>

							<?php

								echo $descript_cours; 
								// $count=strlen($descript_cours);
								// if($count<=157){
								// 	echo $descript_cours;
								// }else{
								// 	$string = substr($descript_cours, 0, 280);
								// 	$end = strlen(strrchr($string, ' ')); 
								// 	$string = substr($string, 0, -$end) . '...';
								// 	echo $string;
								// }

								?>
							
						<?php endif; ?>
						<?php  if (!empty($seats_cours)):?>
							<div class="count"><strong>Осталось мест: </strong><span><?=$seats_cours?></span></div>
						<?php endif; ?>
						<?php  if (!empty($start_cours)):?>
							<div class="date"><span>Старт: </span><strong><?=$start_cours?></strong></div>
						<?php endif; ?>
						<a href="<?php echo get_the_permalink(); ?>">подробнее</a>
					</div>
				</div>
				<?php
					if($counter==2){
						echo '<div class="clearfix visible-lg visible-md"></div>';
					}
					$counter++;
				?>
				<?php } ?>
			</div>
		</div>
	</div>




	<div class="enroll_course" id='enroll_course'>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 double_title">
					<h3 data-title='записаться на курс'>записаться на курс</h3>
				</div>
			</div>

			<div class="row">
				<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
					<?php echo do_shortcode('[contact-form-7 id="40" title="Записаться на курс"]'); ?>
				</div>
			</div>
		</div>
	</div>

	<?php get_footer(); ?>



