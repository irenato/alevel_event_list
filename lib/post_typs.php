<?php
add_action( 'init', 'post_types_init' );

function post_types_init() {


    $labels_object = array(
        'name'               => _x( 'content', 'alevel' ),
        'singular_name'      => _x( 'content', 'alevel' ),
        'menu_name'          => _x( 'CONTENT', 'alevel' ),
        'name_admin_bar'     => _x( 'CONTENT', 'alevel' ),
        'add_new'            => _x( 'Add new','alevel' ),
        'add_new_item'       => __( 'Add new content', 'alevel' ),
        'new_item'           => __( 'New content', 'alevel' ),
        'edit_item'          => __( 'Edit', 'alevel' ),
        'view_item'          => __( 'View', 'alevel' ),
        'all_items'          => __( 'All content', 'alevel' ),
        'search_items'       => __( 'Search content', 'alevel' ),
        'parent_item_colon'  => __( 'Parent content:', 'alevel' ),
        'not_found'          => __( 'Content not found.', 'alevel' ),
        'not_found_in_trash' => __( 'The basket found content.', 'alevel' )
    );

    $args_object = array(
        'menu_icon' => 'dashicons-admin-home',
        'labels'             => $labels_object,
        'description'        => __( 'Description', 'alevel' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'content' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields','page-attributes' )
      //  'taxonomies'         => array('category')
    );

    register_post_type( 'content', $args_object );


    $labels_object = array(
        'name'               => _x( 'courses', 'alevel' ),
        'singular_name'      => _x( 'courses', 'alevel' ),
        'menu_name'          => _x( 'COURSES', 'alevel' ),
        'name_admin_bar'     => _x( 'COURSES', 'alevel' ),
        'add_new'            => _x( 'Add new','alevel' ),
        'add_new_item'       => __( 'Add new courses', 'alevel' ),
        'new_item'           => __( 'New courses', 'alevel' ),
        'edit_item'          => __( 'Edit', 'alevel' ),
        'view_item'          => __( 'View', 'alevel' ),
        'all_items'          => __( 'All courses', 'alevel' ),
        'search_items'       => __( 'Search courses', 'alevel' ),
        'parent_item_colon'  => __( 'Parent courses:', 'alevel' ),
        'not_found'          => __( 'Courses not found.', 'alevel' ),
        'not_found_in_trash' => __( 'The basket found courses.', 'alevel' )
    );

    $args_object = array(
        'menu_icon' => 'dashicons-admin-home',
        'labels'             => $labels_object,
        'description'        => __( 'Description', 'alevel' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'courses' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields','page-attributes' )
       // 'taxonomies'         => array('category')
    );

    register_post_type( 'courses', $args_object );



    $labels_object = array(
        'name'               => _x( 'tidings', 'alevel' ),
        'singular_name'      => _x( 'tidings', 'alevel' ),
        'menu_name'          => _x( 'NEWS', 'alevel' ),
        'name_admin_bar'     => _x( 'NEWS', 'alevel' ),
        'add_new'            => _x( 'Add new','alevel' ),
        'add_new_item'       => __( 'Add new news', 'alevel' ),
        'new_item'           => __( 'New news', 'alevel' ),
        'edit_item'          => __( 'Edit', 'alevel' ),
        'view_item'          => __( 'View', 'alevel' ),
        'all_items'          => __( 'All news', 'alevel' ),
        'search_items'       => __( 'Search news', 'alevel' ),
        'parent_item_colon'  => __( 'Parent news:', 'alevel' ),
        'not_found'          => __( 'News not found.', 'alevel' ),
        'not_found_in_trash' => __( 'The basket found news.', 'alevel' )
    );

    $args_object = array(
        'menu_icon' => 'dashicons-admin-home',
        'labels'             => $labels_object,
        'description'        => __( 'Description', 'alevel' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields','page-attributes' )
        // 'taxonomies'         => array('category')
    );

    register_post_type( 'tidings', $args_object );


 $labels_object = array(
        'name'               => _x( 'partners', 'alevel' ),
        'singular_name'      => _x( 'partners', 'alevel' ),
        'menu_name'          => _x( 'PARTNERS', 'alevel' ),
        'name_admin_bar'     => _x( 'PARTNERS', 'alevel' ),
        'add_new'            => _x( 'Add new','alevel' ),
        'add_new_item'       => __( 'Add new partners', 'alevel' ),
        'new_item'           => __( 'New partners', 'alevel' ),
        'edit_item'          => __( 'Edit', 'alevel' ),
        'view_item'          => __( 'View', 'alevel' ),
        'all_items'          => __( 'All partners', 'alevel' ),
        'search_items'       => __( 'Search partners', 'alevel' ),
        'parent_item_colon'  => __( 'Parent partners:', 'alevel' ),
        'not_found'          => __( 'Partners not found.', 'alevel' ),
        'not_found_in_trash' => __( 'The basket found partners.', 'alevel' )
    );

    $args_object = array(
        'menu_icon' => 'dashicons-admin-home',
        'labels'             => $labels_object,
        'description'        => __( 'Description', 'alevel' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields','page-attributes' )
        // 'taxonomies'         => array('category')
    );

    register_post_type( 'partners', $args_object );


    $labels_object = array(
        'name'               => _x( 'reviews', 'alevel' ),
        'singular_name'      => _x( 'reviews', 'alevel' ),
        'menu_name'          => _x( 'REVIEWS', 'alevel' ),
        'name_admin_bar'     => _x( 'REVIEWS', 'alevel' ),
        'add_new'            => _x( 'Add new','alevel' ),
        'add_new_item'       => __( 'Add new reviews', 'alevel' ),
        'new_item'           => __( 'New reviews', 'alevel' ),
        'edit_item'          => __( 'Edit', 'alevel' ),
        'view_item'          => __( 'View', 'alevel' ),
        'all_items'          => __( 'All reviews', 'alevel' ),
        'search_items'       => __( 'Search reviews', 'alevel' ),
        'parent_item_colon'  => __( 'Parent reviews:', 'alevel' ),
        'not_found'          => __( 'Reviews not found.', 'alevel' ),
        'not_found_in_trash' => __( 'The basket found reviews.', 'alevel' )
    );

    $args_object = array(
        'menu_icon' => 'dashicons-admin-home',
        'labels'             => $labels_object,
        'description'        => __( 'Description', 'alevel' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'reviews' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'custom-fields','page-attributes' )
        //  'taxonomies'         => array('category')
    );

    register_post_type( 'reviews', $args_object );

}