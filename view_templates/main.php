<?php
/**
 * Template name: Main page
 */
?>

<?php get_header();?>
<div class="main_baner">	
	<!-- <img class='leftPoly' src="img/leftPoly.png" alt="">
	<img class='rightPoly' src="img/rightPoly.png" alt=""> -->
	<video autoplay loop id="bgvid">
		<source src="<?=get_template_directory_uri()?>/img/alevel.mp4" type="video/mp4" />
		</video>


		<div class="container">
			<div class="row ">
				<div class="col-lg-9 col-md-10 col-sm-10 col-xs-12 title">

					<p class='hidden-xs'>
						<?php 
						$args=array('post_type'=>'content','p'=>21);
						$query = new wp_Query($args);
						while ($query->have_posts()) {
							$query->the_post();
							$single_post_id = get_the_ID();
							$f_sec_text = get_field('f_sec_text', $single_post_id);

                       // echo "<pre>";
                       // var_dump($f_sec_text);
                       // echo "</pre>";
							if (!empty($f_sec_text)):?>
							<?php echo $f_sec_text; ?>
						<?php endif; ?>

						<?php } ?>

						<div>	
							<a href="#enroll_course">Записаться на курс</a>
						</div>
					</div>
				</div>
			</div>


		</div>


		<div class="about_us" id='aboutUs'>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 double_title">
						<h3 data-title='о нас'>о нас</h3>
					</div>
					<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
						<?php 
						$sec_sec_text = get_field('sec_sec_text', $single_post_id);
						if (!empty($sec_sec_text)):?>
						<?php echo $sec_sec_text; ?>
					<?php endif; ?>
				</div>
			</div>

			<div class="row about_slider">
				<div class="title">Почему <br>стоит<br> менять <br>профессию?</div>
				<div class="col-lg-5 col-md-5 col-sm-6 left-side">
					<ul>
						<li>Неудобный график</li>
						<li>Низкий доход</li>
						<li>Никакой перспективы</li>
						<li>Работа в пределах страны</li>
						<li>Отсутствие вакансий</li>
						<li>Никакого роста</li>
					</ul>

					<div class="itemSlider">
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/mc.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/pol.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/student.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/woman.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/mc.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/pol.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/student.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/woman.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/mc.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/pol.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/student.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/woman.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/mc.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/pol.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/student.png" alt=""></div>
						<div class="item"><img src="<?=get_template_directory_uri()?>/img/woman.png" alt=""></div>
					</div>

				</div>

				<div class="col-lg-2 col-md-2 hidden-sm lineThroght">&nbsp;</div>

				<div class="col-lg-5 col-md-5 col-sm-6 program">
					<img src="<?=get_template_directory_uri()?>/img/it.png" alt="">
					<ul>
						<li>Свободный график</li>
						<li>Высокий доход</li>
						<li>Реальная перспектива</li>
						<li>Работа без границ</li>
						<li>Приятные бонусы</li>
						<li>Постоянный рост</li>
					</ul>

				</div>
			</div>

		</div>
	</div>


	<div class="courses" id='courses'>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 double_title">
					<h3 data-title='наши курсы'>наши курсы</h3>
				</div>
			</div>
			<div class="row">
				<?php 
				$counter=0;
				$args=array('post_type'=>'courses');
				$query = new wp_Query($args);
				while ($query->have_posts()) {
					$query->the_post();
					$single_post_id = get_the_ID();

					$name_cours = get_field('name_cours', $single_post_id);
					$descript_cours = get_field('descript_cours', $single_post_id);
					$seats_cours = get_field('seats_cours', $single_post_id);
					$start_cours = get_field('start_cours', $single_post_id);
					if($counter==3){
								$counter=0;
							}
					?>
					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="item item_1">
							<div class="course_name">курс</div>
							<?php  if (!empty($name_cours)):?>
								<h4 class="title"><?=$name_cours?></h4>
							<?php endif; ?>
							<?php  if (!empty($descript_cours)):?>
								<?php

								echo $descript_cours; 
								// $count=strlen($descript_cours);
								// if($count<=157){
								// 	echo $descript_cours;
								// }else{
								// 	$string = substr($descript_cours, 0, 280);
								// 	$end = strlen(strrchr($string, ' ')); // длина обрезка 
								// 	$string = substr($string, 0, -$end) . '...'; // убираем обрезок добавляем троеточие
								// 	echo $string;
								// }
								?>
								<?php endif; ?>
								<?php  if (!empty($seats_cours)):?>
								<div class="count"><strong>Осталось мест: </strong><span><?=$seats_cours?></span></div>
								<?php endif; ?>
								<?php  if (!empty($start_cours)):?>
								<div class="date"><span>Старт: </span><strong><?=$start_cours?></strong></div>
								<?php endif; ?>
								<a href="<?php echo get_the_permalink(); ?>">подробнее</a>
								</div>
								</div>
								<?php
									if($counter==2){
										echo '<div class="clearfix visible-lg visible-md"></div>';
									}
									$counter++;
								?>

<?php } ?>
</div>
</div>

</div>
<?php $count_tidings = wp_count_posts('tidings');	?>
<?php if($count_tidings->publish!=0):?>

	<div class="news" id='news'>
		<div class="container">
			<div class="row">
				<div class="col-lg-12 double_title">
					<h3 data-title='новости'>новости</h3>
				</div>
			</div>
			<div class="row">
				<?php 
				$args=array('post_type'=>'tidings');
				$query = new wp_Query($args);
				while ($query->have_posts()) {
					$query->the_post();
					$single_post_id = get_the_ID();

					$descript_tidings = get_field('descript_tidings', $single_post_id);
					$date_tidings = get_field('date_tidings', $single_post_id);
					$img_tidings = get_field('img_tidings', $single_post_id);
					$link_tidings = get_field('link_tidings', $single_post_id);
					?>

					<div class="col-lg-4 col-md-4 col-sm-6">
						<div class="item">
							<?php  if (!empty($img_tidings)):?>
								<img src="<?=$img_tidings?>" alt="">
							<?php endif; ?>
							<?php  if (!empty($descript_tidings)):?>
								<?=$descript_tidings?>
							<?php endif; ?>
							<?php  if (!empty($date_tidings)):?>
								<div class="date"><?=$date_tidings?></div>
							<?php endif; ?>
							<?php  if (!empty($link_tidings)):?>
								<a href="<?php echo get_the_permalink();?>"><?=$link_tidings?></a>
							<?php endif; ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	<?php endif;?>

	<?php $count_partners = wp_count_posts('partners');	?>
	<?php if($count_partners->publish!=0):?>
		<div class="partners" id='partners'>
			<div class="container">
				<div class="row">
					<div class="col-lg-12 double_title">
						<h3 data-title='Партнеры'>Партнеры</h3>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="sliderPartners">
							<?php 
							$args=array('post_type'=>'partners');
							$query = new wp_Query($args);
							while ($query->have_posts()) {
								$query->the_post();
								$single_post_id = get_the_ID();
								$m_partners = get_field('m_partners', $single_post_id);
								for($i=0;$i<count($m_partners);$i++){?>
								<?php if (!empty($m_partners[$i]['img_partners'])): ?>
									<div class="item"><a href="<?=$m_partners[$i]['link_partners']?>"><img src="<?=$m_partners[$i]['img_partners']?>" alt=""></a></div>
								<?php endif; ?>
								<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif;?>

		<?php $count_reviews = wp_count_posts('reviews');?>
		<?php if($count_reviews->publish!=0):?>
			<div class="reviews" id='reviews'>
				<div class="container">
					<div class="row">
						<div class="col-lg-12 double_title">
							<h3 data-title='отзывы'>отзывы</h3>
						</div>
					</div>

					<div class="row">
						<?php 
						$args=array('post_type'=>'reviews');
						$query = new wp_Query($args);
						while ($query->have_posts()) {
							$query->the_post();
							$single_post_id = get_the_ID();

							$name_rev = get_field('name_rev', $single_post_id);
							$url_rev = get_field('url_rev', $single_post_id);
							$cours_rev= get_field('cours_rev', $single_post_id);
							$text_rev = get_field('text_rev', $single_post_id);
							$img_rev = get_field('img_rev', $single_post_id);
							?>

							<div class="col-lg-6 col-md-6 col-sm-6 item">

								<?php  if (!empty($img_rev)):?>
									<span class="reviewImage"><img src="<?=$img_rev?>" alt=""></span>
								<?php endif; ?>
								<div class="course_name">
									<?php  if (!empty($name_rev)):?>
										<div class="name"><?= $name_rev?></div>
										<br>
									<?php endif; ?>
									<?php  if (!empty($name_rev) && !empty($url_rev)):?>
										<a href="<?=$url_rev?>"><?=$cours_rev?></a>
									<?php endif; ?>
								</div>
								<?php  if (!empty($text_rev)):?>
									<?=$text_rev?>
								<?php endif; ?>
							</div>
							<?php } ?>
						</div>
						<div class="row ">
							<a class='write_review' href="">написать отзыв</a>
						</div>
					</div>
				</div>
			<?php endif;?>

			<div class="enroll_course" id='enroll_course'>
				<div class="container">
					<div class="row">
						<div class="col-lg-12 double_title">
							<h3 data-title='записаться на курс'>записаться на курс</h3>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
							<?php echo do_shortcode('[contact-form-7 id="40" title="Записаться на курс"]'); ?>
						</div>
					</div>
				</div>
			</div>

			<?php 
  // echo "<pre>";
  //                      var_dump($count_reviews->publish);
  //                      echo "</pre>";

			?>
			<?php get_footer(); ?>