jQuery(document).ready(function($) {
	$('#form_quiz').validate({
		submitHandler: function() {
			var answer_str=Score();
			var name= $('input[name=name]').val();
			var phone=$('input[name=phone]').val();
			var email=$('input[name=email]').val();
			// console.log(name);
			$.ajax({
				url: alevel_ajax.ajax_url,
				type: 'POST',
				data: {
					'action': 'mess_test',
					'ans_str': answer_str,
					'user_name': name,
					'user_phone': phone,
					'user_email': email
				},
				success: function (data) {
					swal('Форма успешно отправлена');
				}
			});
			// console.log(answer_str);
		},
		rules:{
			name:{
				required : true,
				minlength: 2
			},
			phone:{
				required : true,
				number:true
			},
			email:{
				required : true,
				email:true
			}
		},
		messages:{
			name:{
				required : "Это поле обязательно",
				minlength: "Надо две буковки вписать"
			},
			phone:{
				required : "Это поле обязательно",
				number: "Только цифры"
			},
			email:{
				required : "Это поле обязательно",
				email:"E-mail введен некоректно"
			}
		}
	});
});